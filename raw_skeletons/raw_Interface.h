#pragma once

#include "GaudiKernel/IAlgTool.h"

${comment}

class ${name} : virtual public IAlgTool {
public:
  /// InterfaceID
  DeclareInterfaceID( ${name}, 1, 0 );

  // ... interface methods ...

};
