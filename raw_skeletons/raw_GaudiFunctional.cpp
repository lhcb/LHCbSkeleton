#include "${name}.h"

//---------------------------------------------------------------------------
// Implementation file for class : ${name}
//
// ${date} : ${author}
//---------------------------------------------------------------------------

DECLARE_COMPONENT( ${name} )

//===========================================================================
// operator () implementation
//===========================================================================
${GaudiFunctionalOutput} ${name}::operator()(${GaudiFunctionalInput}${ref}) const {
  ${operatorParenText}
}
