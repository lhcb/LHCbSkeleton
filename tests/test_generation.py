#!/usr/bin/env python

import os
import shutil

from subprocess import check_call

from MakeLHCbCppClass import make_files


class Options(object):
    def __init__(self, **kwargs):
        self.type = kwargs.get('type')
        self.GaudiFunctional = kwargs.get('GaudiFunctional')
        self.DaVinciAlgorithmType = kwargs.get('DaVinciAlgorithmType')
        self.AlgorithmType = kwargs.get('AlgorithmType')
        self.Interface = kwargs.get('Interface')
        self.Tool = kwargs.get('Tool')
        self.GaudiFunctionalInput = kwargs.get('GaudiFunctionalInput')
        self.GaudiFunctionalOutput = kwargs.get('GaudiFunctionalOutput')
        self.write = kwargs.get('write')
        self.GFInheritance = kwargs.get('GFInheritance')

        self.isTTY = kwargs.get('isTTY', False)

    def desc(self):
        return str(dict(item for item in self.__dict__.items() if item[1]))

    def clone(self):
        return Options(**self.__dict__)

    def genName(self):
        return 'TestTemplate_' + '_'.join(
            str(i) for i in [self.type,
                             self.AlgorithmType,
                             self.DaVinciAlgorithmType,
                             self.GaudiFunctional,
                             self.GFInheritance,
                             self.Interface] if i)

def make_proj(cwd=os.path.curdir):
    '''
    Generate a project to build generated files.
    '''
    if not os.path.isdir(cwd):
        os.makedirs(cwd)
    elif os.path.exists(os.path.join(cwd, 'TestProj')):
        shutil.rmtree(os.path.join(cwd, 'TestProj'))
    check_call(['lb-dev', '--name', 'TestProj', 'DaVinci/latest'], cwd=cwd)
    os.makedirs(os.path.join(cwd, 'TestProj', 'TestPkg', 'src'))
    with open(os.path.join(cwd, 'TestProj', 'TestPkg', 'CMakeLists.txt'), 'w') as cfg:
        cfg.write('find_package(Boost)\n')
        cfg.write('find_package(ROOT)\n')
        cfg.write('include_directories(SYSTEM ${Boost_INCLUDE_DIRS} '
                  '${ROOT_INCLUDE_DIRS})\n')
        cfg.write('add_definitions(-DOUTPUT_TYPE=DataObject -DINPUT_TYPE=DataObject '
                  '-DOUTPUT_TYPE1=DataObject -DOUTPUT_TYPE2=DataObject '
                  '-DINPUT_TYPE1=DataObject -DINPUT_TYPE2=DataObject)\n')
        cfg.write('gaudi_add_module(TestPkg src/*.cpp '
                  'LINK_LIBRARIES DaVinciKernelLib)\n')


COMBINATIONS = [Options(type='A', AlgorithmType='N'),
                Options(type='A', AlgorithmType='H'),
                Options(type='A', AlgorithmType='T'),
                Options(type='DVA', DaVinciAlgorithmType='N'),
                Options(type='DVA', DaVinciAlgorithmType='H'),
                Options(type='DVA', DaVinciAlgorithmType='T'),
                Options(type='GFA', GaudiFunctional='T'),
                Options(type='GFA', GaudiFunctional='P'),
                Options(type='GFA', GaudiFunctional='C'),
                Options(type='GFA', GaudiFunctional='M'),
                Options(type='GFA', GaudiFunctional='T', GFInheritance='GaudiHistoAlg'),
                Options(type='GFA', GaudiFunctional='P', GFInheritance='GaudiHistoAlg'),
                Options(type='GFA', GaudiFunctional='C', GFInheritance='GaudiHistoAlg'),
                Options(type='GFA', GaudiFunctional='M', GFInheritance='GaudiHistoAlg'),
                Options(type='T'),
                Options(type='T', Interface=Options(type='I').genName()),
                Options(type='I'),
                Options(type='S'),
                ]


def test_generation():
    for opts in COMBINATIONS:
        opts = opts.clone()
        def maker(*args):
            make_files(*args)
        maker.description = opts.desc()
        yield maker, opts, opts.genName() + '.h'
        if opts.type != 'I':
            yield maker, opts, opts.genName() + '.cpp'

def test_compilation():
    make_proj()
    start_dir = os.getcwd()
    os.chdir(os.path.join('TestProj', 'TestPkg', 'src'))
    for opts in COMBINATIONS:
        opts = opts.clone()
        opts.write = True
        name = opts.genName()
        make_files(opts, name + '.h')
        if opts.type != 'I':
            make_files(opts, name + '.cpp')
        else:
            with open( name + '.cpp', 'w') as f:
                f.write('#include "{0}.h"\n'.format(opts.genName()))
    os.chdir(start_dir)
    check_call(['make', 'BUILDFLAGS=-j4 -k0'], cwd='TestProj')
