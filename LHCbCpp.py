#!/usr/bin/python
import sys,os,pwd,time
from string import Template
from support import comment

class LHCbCpp:
    def __init__(self, name,configs = None, requirements = None):
        self.name = name
        self.configs = configs
        self.configs.name = name
        self.requirements = requirements
        self.configs.date = time.strftime("%Y-%m-%d")
        self.configs.author = (pwd.getpwuid(os.getuid())[4]).split(',')[0]
        curr_path = os.path.dirname(os.path.abspath(__file__))
        if self.configs.type =='GFA':
            if self.configs.GaudiFunctional=='Producer':
                self.configs.GaudiFunctionalInput = ''
                self.configs.ref = ''
            else:
                self.configs.ref = '&'
            the_string = open(curr_path+'/raw_skeletons/raw_GaudiFunctional.cpp','r').read()
            self.configs.operatorParenText = self.configs.GaudiFunctionalOutput + ' ret; return ret;'
            if self.configs.GaudiFunctional=='Consumer':
                self.configs.operatorParenText = 'return;'
        elif self.configs.type == 'T':
            the_string = open(curr_path+'/raw_skeletons/raw_Tool.cpp','r').read()
            if not self.configs.Interface==None:
                self.configs.ToolBase = 'base_class'
            else:
                self.configs.ToolBase = 'GaudiTool'
        elif self.configs.type == 'I':
            pass
            #the_string = open(curr_path+'/raw_skeletons/raw_Interface.cpp','r').read()
        elif self.configs.type == 'DVA':
            the_string = open(curr_path+'/raw_skeletons/raw_DaVinciAlgorithm.cpp','r').read()
            if self.configs.DaVinciAlgorithmType=='Normal':
                self.configs.DaVinciAlgorithmTypeName=''
            else:
                self.configs.DaVinciAlgorithmTypeName = self.configs.DaVinciAlgorithmType
        elif self.configs.type == 'A':
            if self.configs.AlgorithmType == "Normal":
                self.configs.AlgorithmTypeName='Algorithm'
            else:
                self.configs.AlgorithmTypeName = self.configs.AlgorithmType+'Alg'
            the_string = open(curr_path+'/raw_skeletons/raw_Algorithm.cpp','r').read()
        else:
            the_string = open(curr_path+'/raw_skeletons/raw_class.cpp','r').read()
        temp = Template(the_string)
        self.genText =  temp.safe_substitute(vars(self.configs))

